# How To Add Member

Adding member require the `maintainer` or `owner` role.

## Steps

1. Go to the project
2. Select the menu

![Adding Member Image 1](images/adding-member-image-1.png)

3. Find the user's email or username and select the role, then add

![Adding Member Image 2](images/adding-member-image-2.png)
