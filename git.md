# Git Cheatsheet

## Listing Branch

```
git branch
```

If the branch you want doesn't appear, but already in the remote, it might be caused that your index is not updated with the server yet, so run

```
git fetch
```

then do git branch again.

## Change Branch

```
git checkout <branch-name>
```

## Create New Branch

This one will only create the branch:

```
git branch <branch-name>
```

This one will create the branch and checkout it

```
git checkout -b <branch-name>
```

## Commit And Push Code

1. First, add the files

```
git add <file-name>
```

or if you lazy, and want to add everything, just use this

```
git add .
```

2. Commit it

```
git commit -m "<commit message>"
```

3. Push it

```
git push origin HEAD
```

## In case you accidentally commit something

Cancelling one last commit:

```
git reset HEAD~1
```

Cancelling two last commit:

```
git reset HEAD~2
```

and you know the rest

## In case you want to discard changes:

discard one file

```
git checkout -- <file-name>
```

discard everything

```
git checkout -- .
```

## Merge:

```
git merge <branch-name>
```

in case the merge got conflict, fix the conflicts first, then after that

```
git add <file-name>
git merge --continue
```

you will be prompted to enter merge message in vim, in most cases just use this combination of key to close and save the vim editor in succession:

1. ctrl + c
2. :
3. w
4. q
5. !
6. enter

so it should looks like this:

![vim](images/vim.png)

Explanation:

- ctrl + c = activate command mode in vim
- : = command set
- w = write
- q = quit
- ! = (i forgot what is this, it just always like this)

## Taking Code From Other Branch Or Commit

```
git checkout <branch-name or commit-hash> -- <file-name>
```

example:

```
git checkout master -- app.js
```

## Cleaning Local Branch Except Master

**Warning!!** Be careful, this command will remove all your local branches. Means that your work that is only in local will be gone!

Windows:

```
git checkout master
git branch -D  @(git branch | select-string -NotMatch "master,development" | Foreach {$_.Line.Trim()})
```
