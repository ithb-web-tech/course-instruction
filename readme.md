# ITHB Web Technology Course

Welcome to ITHB's Web Technology course. I will provide the documentation on how to get started with the course. Please follow the instruction and install all the prerequisites before attending the course.

## Prerequisites

Due to the internet requirement, please download and install the required softwares. The following guide will be mainly for windows, any other platform might be different, but still fundamentally the same.

If you already understand the installation, it's okay to improvise according to your needs, but this guide will still be assuming the reader doesn't know anything at all.

- [PostgreSQL](https://www.postgresql.org/): Please remember the password you put for the `postgres` user
- [NodeJS](https://nodejs.org/en/) recommended to use the latest LTS, not the current, but should be fine either way.
- NPM: already packed together with NodeJS
- [Git](https://git-scm.com/downloads)

Another required, but can be changed using other similar software

- Database explorer, at least install one of the following:
  - [HeidiSQL](https://www.heidisql.com/): Database explorer for most of the popular database system
  - [pgAdmin 4](https://www.pgadmin.org/download/pgadmin-4-windows/): Database explorer for postgresql
- Code Editor or IDE, at least install of the following or can use any other that suits you well:
  - [Visual Studio Code](https://code.visualstudio.com/) (Most recommended)
  - [Atom](https://atom.io/) (warning: resource hungry!)
- REST API Client: this or any other that suits you well
  - [Postman](https://www.getpostman.com/downloads/)

Make sure to install all of them and test if they are correctly configured.

## Checking Installation

This section will guide you on how to check if the installation are correct.

### NodeJS

1. Open terminal / command line / power shell
1. Type the following command: `node --version`, you should see version of the NodeJS, for example: `v10.16.0`
1. Test if the NPM installed correctly, type the following command: `npm --version`, you should be able to see the version of npm, in my case is `6.9.0`
1. Test if the npm global is already pointing to the correct executable directories. Install this package: `npm -g i nodemon`
1. Execute the following command `nodemon --version`, you should get the version, in my case is `1.19.1`

### Git

1. Open terminal / command line / power shell
1. Type the following command: `git --version`, you should see the version of the git, for example: `git version 2.22.0.windows.1`

### PostgreSQL

The following guide will try to connect using HeidiSQL

1. Open HeidiSQL
1. Create New Connection
1. Select `Network Type` to become `PostgreSQL (TCP/IP)`
1. Make sure the user is `postgres`
1. Insert the password defined during installation
1. Press open, if the connection went well, it will prompt you to save the connection, just save it for convenience

## Getting Started

The course will be started with some introduction to the web technology used. In this semester, we will be using React and ExpressJS. I already provided the clean template, please clone both of this repository before the class begin.

- [Raw React UI](https://gitlab.com/ithb-web-tech/raw-react-ui): `git clone https://gitlab.com/ithb-web-tech/raw-react-ui.git`
- [Raw Express Api Server](https://gitlab.com/ithb-web-tech/raw-express-api-server): `git clone https://gitlab.com/ithb-web-tech/raw-express-api-server.git`

Please follow the installation instruction in each of the repository as it will require internet connection

# Useful Cheatsheet

- [Gitlab Member Management](gitlab-member.md)
- [Basic Git Cheatsheet](git.md)
